### to run:
  ```./gradlew clean fatjar && java -jar build/libs/exercise-all-1.0-SNAPSHOT.jar ./input_rovers.txt```

### to test:
  ```gradle cleanTest test```

### notes
* I haven't used any kind of synchronization, since I think it's not one of the goals
* assumed that drones don't block one another
* assumed that there always has to be at least one command per rover. If there's no command, there's no point to send anything to a rover
* Even though the test text says I shouldn't use any external libraries, I've used logback for better debugging. For this use case, it doesn't give much more functionality than a `System.out.println` would give
* It'd be more practical to have a properties file where one can configure params
