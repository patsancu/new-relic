package com.new_relic.challenge;

import com.new_relic.challenge.controller.MarsRoverCommander;
import com.new_relic.challenge.controller.RoverCommander;
import com.new_relic.challenge.exception.BadInputException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

//TODO PSC: Put in proper package
public class Main {
    static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        if (args.length != 1) {
            logger.error("You should provide *one* input file");
            System.exit(0);
        }

        String fileName = args[0];
        logger.debug("fileName is :" + fileName);

        try (
                FileReader fileReader = new FileReader(new File(fileName));
                BufferedReader bufferedReader = new BufferedReader(fileReader)
        ) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                logger.info("------------");
                logger.info("Input line: ");
                logger.info("------------");
                logger.info(line);
                RoverCommander marsRoverCommander = new MarsRoverCommander();
                marsRoverCommander.loadCommandsFromInput(line);
                marsRoverCommander.executeAllCommands();
                final String finalRoverLocation = marsRoverCommander.getRoversLocationString();
                logger.info("------------");
                logger.info("Output line:");
                logger.info("------------");
                logger.info(finalRoverLocation);
            }
        } catch (FileNotFoundException ex) {
            logger.error("Unable to open file '" + fileName + "'. It doesn't exist");
        } catch (IOException ex) {
            logger.error("Error reading file: '" + fileName + "'");
        } catch (BadInputException e) {
            logger.error("Bad info when reading file: '" + fileName + "'");
            e.printStackTrace();
        }
    }
}
