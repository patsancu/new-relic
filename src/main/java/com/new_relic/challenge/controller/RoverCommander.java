package com.new_relic.challenge.controller;

import com.new_relic.challenge.exception.BadInputException;
import com.new_relic.challenge.domain.Rover;
import com.new_relic.challenge.domain.utils.Command;

import java.util.ArrayList;
import java.util.List;

public abstract class RoverCommander {

    protected List<Command> commands;
    protected List<Rover> roverList;
    protected int nextUnappliedCommand = 0;

    public abstract String getRoversLocationString();
    public abstract void addCommandsFromInput(String commandInput) throws BadInputException;


    public void executeRemainingCommands() {
        while (nextUnappliedCommand < commands.size()) {
            executeNextCommand();
            nextUnappliedCommand += 1;
        }
    }

    public void executeNextCommand() {
        final Command command = commands.get(nextUnappliedCommand);
        command.execute();
    }

    public void executeInput(String input) throws BadInputException {
        loadCommandsFromInput(input);
        executeAllCommands();
    }

    public void executeAllCommands() {
        this.nextUnappliedCommand = 0;
        executeRemainingCommands();
    }

    public void loadCommandsFromInput(String commandInput) throws BadInputException {
        roverList = new ArrayList<>();
        commands = new ArrayList<>();
        addCommandsFromInput(commandInput);
    }
}
