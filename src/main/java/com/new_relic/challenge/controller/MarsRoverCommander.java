package com.new_relic.challenge.controller;

import com.new_relic.challenge.exception.BadInputException;
import com.new_relic.challenge.domain.Plateau;
import com.new_relic.challenge.domain.Rover;
import com.new_relic.challenge.domain.utils.Command;
import com.new_relic.challenge.domain.utils.Direction;
import com.new_relic.challenge.domain.utils.Action;
import com.new_relic.challenge.domain.mars.actions.MarsMoveForward;
import com.new_relic.challenge.domain.mars.actions.MarsRotate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

public class MarsRoverCommander extends RoverCommander {
    static Logger logger = LoggerFactory.getLogger(MarsRoverCommander.class);

    private static int REQUIRED_NUMBER_OF_TOKENS = 4; //number of tokens required for a rover


    /**
     * The first snippet of input is the upper-right coordinates of the plateau,
     */
    private static int BEGINNING_OFFSET = 2; // number of params required for the plateau

    @Override
    public String getRoversLocationString() {
        String locationString = "";
        for (Rover marsRover : roverList) {
            locationString += getLocationString(marsRover);
            locationString += " ";
        }
        return locationString.trim();
    }


    @Override
    public void addCommandsFromInput(String commandInput) throws BadInputException {
        String[] tokens = commandInput.split(" ");
        if (tokens.length < 5) {
            throw new BadInputException("Not enough tokens in input");
        }
        try {
            // in the exercise text, it says the first snippet are the upper right coordinates
            // so the coordinates the rovers can be in the range from 0 to N, which gives dimensions of N+1
            final Plateau plateau = parsePlateau(tokens);
            int numberOfRovers = tokens.length / REQUIRED_NUMBER_OF_TOKENS;
            for (int i = 0; i < numberOfRovers; i++) {
                final int offset = BEGINNING_OFFSET + 4 * i;
                final Rover marsRover = parseRover(tokens, plateau, offset);
                roverList.add(marsRover);
                final String commandTokens = tokens[offset + 3];
                logger.debug("Commands: " + commandTokens);
                for (int commandIndex = 0; commandIndex < commandTokens.length(); commandIndex++) {
                    final String commandRaw = commandTokens.substring(commandIndex, commandIndex + 1); // this is, assuming all commands are ONE char long
                    final Command command = parseCommand(commandRaw, marsRover);
                    commands.add(command);
                }
            }
        } catch (NumberFormatException nfe) {
            throw new BadInputException("Some token that was supposed to be a number couldn't be parsed as such");
        } catch (IllegalFormatException e) {
            throw new BadInputException("Some token that was supposed to be a direction couldn't be parsed as such");
        }
    }

    private Rover parseRover(String[] tokens, Plateau plateau, int offset) {
        final int initialX = Integer.parseInt(tokens[offset + 0]);
        final int initialY = Integer.parseInt(tokens[offset + 1]);
        final Direction initialDirection = Direction.valueOf(tokens[offset + 2]);
        logger.debug("Initial coordinates: x->" + initialX + "; y->" + initialY + ". Initial direction: " + initialDirection);
        return Rover.RoverBuilder.aRover()
                .withDirection(initialDirection)
                .withX(initialX)
                .withY(initialY)
                .withPlateau(plateau)
                .build();
    }

    private Plateau parsePlateau(String[] tokens) throws BadInputException {
        final int upperX = Integer.parseInt(tokens[0] + 1);
        final int upperY = Integer.parseInt(tokens[1] + 1);
        if ((tokens.length - BEGINNING_OFFSET) % REQUIRED_NUMBER_OF_TOKENS != 0) {
            throw new BadInputException("Some token that was supposed to be a number couldn't be parsed as such");
        }
        final Plateau plateau = new Plateau(upperY, upperX);
        return plateau;
    }

    private Command parseCommand(String firstToken, Rover rover) {
        Command command = new Command();
        if (firstToken.equals("R") || firstToken.equals("L")) {
            List<String> attributes = new ArrayList<>();
            attributes.add(firstToken);
            Action action = new MarsRotate();
            command = Command.CommandBuilder
                    .aCommand()
                    .withAction(action)
                    .withAttributes(attributes)
                    .withRover(rover)
                    .build();
        } else if (firstToken.equals("M")) {
            List<String> attributes = new ArrayList<>();
            Action action = new MarsMoveForward();
            command = Command.CommandBuilder
                    .aCommand()
                    .withAction(action)
                    .withAttributes(attributes)
                    .withRover(rover)
                    .build();
        }
        return command;
    }

    private String getLocationString(Rover marsRover) {
        final int xPosition = marsRover.getX();
        final int yPosition = marsRover.getY();
        final Direction direction = marsRover.getDirection();
        final String locationString = String.format("%s %s %s", xPosition, yPosition, direction);
        return locationString;
    }
}
