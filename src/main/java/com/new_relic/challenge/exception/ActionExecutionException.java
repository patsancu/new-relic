package com.new_relic.challenge.exception;

public class ActionExecutionException extends Exception {
    public ActionExecutionException() {
    }

    public ActionExecutionException(Throwable cause) {
        super(cause);
    }

    public ActionExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActionExecutionException(String message) {
        super(message);
    }
}
