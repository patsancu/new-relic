package com.new_relic.challenge.exception;


public class BadInputException extends Throwable {
    public BadInputException() {
    }

    public BadInputException(Throwable cause) {
        super(cause);
    }

    public BadInputException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadInputException(String message) {
        super(message);
    }

}
