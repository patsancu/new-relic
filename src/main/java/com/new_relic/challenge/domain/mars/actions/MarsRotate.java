package com.new_relic.challenge.domain.mars.actions;

import com.new_relic.challenge.domain.Rover;
import com.new_relic.challenge.domain.utils.Direction;
import com.new_relic.challenge.domain.utils.Steering;
import com.new_relic.challenge.domain.utils.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MarsRotate extends Action {
    static Logger logger = LoggerFactory.getLogger(MarsRotate.class);

    @Override
    public void execute(Action action, List<String> attributes, Rover rover) {
        Steering steering = Steering.valueOf(attributes.get(0));
        Direction direction = rover.getDirection();
        Direction newDirection = Direction.UNKNOWN;
        switch (steering) {
            case R:
                switch (direction) {
                    case N:
                        newDirection = Direction.E;
                        break;
                    case E:
                        newDirection = Direction.S;
                        break;
                    case S:
                        newDirection = Direction.W;
                        break;
                    case W:
                        newDirection = Direction.N;
                        break;
                }
                break;
            case L:
                switch (direction) {
                    case N:
                        newDirection = Direction.W;
                        break;
                    case W:
                        newDirection = Direction.S;
                        break;
                    case S:
                        newDirection = Direction.E;
                        break;
                    case E:
                        newDirection = Direction.N;
                        break;
                }
                break;
        }
        logger.debug("Rotated *{}* from '{}' to '{}'", steering.getWholeName(), direction.getWholeName(), newDirection.getWholeName());
        rover.setDirection(newDirection);
    }
}
