package com.new_relic.challenge.domain.mars.actions;

import com.new_relic.challenge.exception.ActionExecutionException;
import com.new_relic.challenge.domain.Rover;
import com.new_relic.challenge.domain.utils.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MarsMoveForward extends Action {
    static Logger logger = LoggerFactory.getLogger(MarsMoveForward.class);

    @Override
    public void execute(Action action, List<String> attributes, Rover rover) throws ActionExecutionException {
        final int yMovement = getNextY(rover);
        final int xMovement = getNextX(rover);
        if (moveIsLegal(rover, xMovement, yMovement)) {
            logger.debug("Will move forward");
            rover.setX(rover.getX() + xMovement);
            rover.setY(rover.getY() + yMovement);
        } else {
            logger.info("The rover didn't advance because it would fall");
            throw new ActionExecutionException("The rover didn't advance because it would fall");
        }
    }

    private boolean moveIsLegal(Rover rover, int xMovement, int yMovement) {
        final int newX = rover.getX() + xMovement;
        final int newY = rover.getY() + yMovement;
        if (newX < rover.getPlateau().getWidth() && newX >= 0) {
            if (newY < rover.getPlateau().getHeight() && newY >= 0) {
                return true;
            }
        }
        return false;
    }

    private int getNextY(Rover rover) {
        switch (rover.getDirection()) {
            case N:
                return 1;
            case S:
                return -1;
        }
        return 0;
    }

    private int getNextX(Rover rover) {
        switch (rover.getDirection()) {
            case E:
                return 1;
            case W:
                return -1;
        }
        return 0;
    }
}
