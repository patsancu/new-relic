package com.new_relic.challenge.domain;

import com.new_relic.challenge.domain.utils.Direction;

public class Rover {
    protected int x;
    protected int y;
    protected Direction direction;
    protected Plateau plateau;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }


    public static final class RoverBuilder {
        protected int x;
        protected int y;
        protected Direction direction;
        protected Plateau plateau;

        private RoverBuilder() {
        }

        public static RoverBuilder aRover() {
            return new RoverBuilder();
        }

        public RoverBuilder withX(int x) {
            this.x = x;
            return this;
        }

        public RoverBuilder withY(int y) {
            this.y = y;
            return this;
        }

        public RoverBuilder withDirection(Direction direction) {
            this.direction = direction;
            return this;
        }

        public RoverBuilder withPlateau(Plateau plateau) {
            this.plateau = plateau;
            return this;
        }

        public Rover build() {
            Rover rover = new Rover();
            rover.setX(x);
            rover.setY(y);
            rover.setDirection(direction);
            rover.setPlateau(plateau);
            return rover;
        }
    }

    @Override
    public String toString() {
        return "Rover{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                ", plateau=" + plateau +
                '}';
    }
}
