package com.new_relic.challenge.domain.utils;

public enum Direction {
    N ("North"),
    S ("South"),
    W ("West"),
    E ("East"),
    UNKNOWN ("Unknown");

    private String wholeName;

    Direction(String wholeName) {
        this.wholeName = wholeName;
    }

    public String getWholeName() {
        return wholeName;
    }
}
