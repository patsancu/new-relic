package com.new_relic.challenge.domain.utils;

import com.new_relic.challenge.exception.ActionExecutionException;
import com.new_relic.challenge.domain.Rover;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Command {
    static Logger logger = LoggerFactory.getLogger(Command.class);

    protected Action action;
    protected List<String> attributes;
    protected Rover rover;

    public Command() {

    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public Rover getRover() {
        return rover;
    }

    public void setRover(Rover rover) {
        this.rover = rover;
    }

    public void execute() {
        try {
            action.execute(action, attributes, rover);
        } catch (ActionExecutionException e) {
            logger.warn("There was an error when trying to execute the action: {} with params: {} for rover: {}",
                    action.getClass(), attributes, rover);
            logger.warn(e.getMessage());
        }
    }

    public static final class CommandBuilder {
        protected Action action;
        protected List<String> attributes;
        protected Rover rover;

        private CommandBuilder() {
        }

        public static CommandBuilder aCommand() {
            return new CommandBuilder();
        }

        public CommandBuilder withAction(Action action) {
            this.action = action;
            return this;
        }

        public CommandBuilder withAttributes(List<String> attributes) {
            this.attributes = attributes;
            return this;
        }

        public CommandBuilder withRover(Rover rover) {
            this.rover = rover;
            return this;
        }

        public Command build() {
            Command command = new Command();
            command.setAction(action);
            command.setAttributes(attributes);
            command.setRover(rover);
            return command;
        }
    }
}