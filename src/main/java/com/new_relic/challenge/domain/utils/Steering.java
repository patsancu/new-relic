package com.new_relic.challenge.domain.utils;

public enum Steering {
    L("Left"),
    R("Right");

    private String wholeName;

    Steering(String wholeName) {
        this.wholeName = wholeName;
    }

    public String getWholeName() {
        return wholeName;
    }
}
