package com.new_relic.challenge.domain.utils;

import com.new_relic.challenge.exception.ActionExecutionException;
import com.new_relic.challenge.domain.Rover;

import java.util.List;

public abstract class Action {
    public abstract void execute(Action action, List<String> attributes, Rover rover) throws ActionExecutionException;

    public Action() {

    }
}