import com.new_relic.challenge.exception.BadInputException;
import com.new_relic.challenge.controller.MarsRoverCommander;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class MarsRoverCommanderTest {
    MarsRoverCommander marsRoverCommander;
    final Random random = new Random();

    @Before
    public void initialize() {
        marsRoverCommander = new MarsRoverCommander();
    }

    @Test
    public void testExerciseInputOutputisOk() throws BadInputException {
        // given
        final String fileInput = "5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM";

        // when
        marsRoverCommander.executeInput(fileInput);

        // then
        assertEquals(marsRoverCommander.getRoversLocationString(), "1 3 N 5 1 E");
    }

    @Test
    public void testFullRotationRight() throws BadInputException {
        // given
        final int width = 1;
        final int height = 9;
        final int initialX = 1;
        final int initialY = 2;
        final String initialDirection = "N";
        final String commands = generateFullRotationRandomNumberCommands("R");
        final String fileInput = String.format("%d %d %d %d %s %s", width, height, initialX, initialY, initialDirection, commands);

        // when
        System.out.println(commands);
        marsRoverCommander.executeInput(fileInput);

        // then
        final String expectedLocation = String.format("%d %d %s", initialX, initialY, initialDirection);
        assertEquals("rover should have had a full rotation", marsRoverCommander.getRoversLocationString(), expectedLocation);
    }

    @Test
    public void testFullRotationLeft() throws BadInputException {
        // given
        final int width = 1;
        final int height = 9;
        final int initialX = 1;
        final int initialY = 2;
        final String initialDirection = "S";
        final String commands = generateFullRotationRandomNumberCommands("L");
        final String fileInput = String.format("%d %d %d %d %s %s", width, height, initialX, initialY, initialDirection, commands);

        // when
        marsRoverCommander.executeInput(fileInput);

        // then
        final String expectedLocation = String.format("%d %d %s", initialX, initialY, initialDirection);
        assertEquals("rover should have had a full rotation", marsRoverCommander.getRoversLocationString(), expectedLocation);
    }

    @Test
    public void testRoverDoesntFallFromPlateau() throws BadInputException {
        // given
        final String fileInput = "5 1 0 0 S M";

        // when
        marsRoverCommander.executeInput(fileInput);

        // then
        assertEquals(marsRoverCommander.getRoversLocationString(), "0 0 S");
    }

    @Test
    public void testRoverTurnsBackAndForth() throws BadInputException {
        // given
        final String fileInput = "5 1 0 0 S RL";

        // when
        marsRoverCommander.executeInput(fileInput);

        // then
        assertEquals(marsRoverCommander.getRoversLocationString(), "0 0 S");
    }

    @Test
    public void testRoverThinVerticallPlateauEndToEnd() throws BadInputException {
        // given
        final int width = 1;
        final int height = 9;
        final String rover1Position = "0 0 N";
        final String rover2Position = "0 " + height + " S";
        final String rover1Commands = generateNforwardCommand(height);
        final String rover2Commands = generateNforwardCommand(height);
        final String rover1 = String.format("%s %s", rover1Position, rover1Commands);
        final String rover2 = String.format("%s %s", rover2Position, rover2Commands);
        final String fileInput = String.format("%d %d %s %s", width, height, rover1, rover2);

        // when
        marsRoverCommander.executeInput(fileInput);

        // then
        final String expectedLocation = String.format("0 %d N 0 0 S", height);
        final String actualLocation = marsRoverCommander.getRoversLocationString();
        assertEquals("one rover should be at the top of the plateau facing north and the other one should be at the bottom facing south", actualLocation, expectedLocation);
    }

    @Test
    public void testRoverThinHorizontalPlateauEndToEnd() throws BadInputException {
        // given
        final int width = 100;
        final int height = 1;
        final String rover1Position = "0 0 E";
        final String rover2Position = width + " 0 W";
        final String rover1Commands = generateNforwardCommand(width);
        final String rover2Commands = generateNforwardCommand(width);
        final String rover1 = String.format("%s %s", rover1Position, rover1Commands);
        final String rover2 = String.format("%s %s", rover2Position, rover2Commands);
        final String fileInput = String.format("%d %d %s %s", width, height, rover1, rover2);

        // when
        marsRoverCommander.executeInput(fileInput);

        // then
        final String expectedLocation = String.format("%d 0 E 0 0 W", width);
        final String actualLocation = marsRoverCommander.getRoversLocationString();
        assertEquals("one rover should be at the right facing east and the other should be on the left facing west and the other ", actualLocation, expectedLocation);
    }

    @Test
    public void testThreeRovers() throws BadInputException {
        // given
        final String fileInput = "5 5 1 2 N L 3 3 E L 0 0 N R";

        // when
        marsRoverCommander.executeInput(fileInput);

        // then
        assertEquals(marsRoverCommander.getRoversLocationString(), "1 2 W 3 3 N 0 0 E");
    }

    private String generateFullRotationRandomNumberCommands(String direction) {
        String commands = "";
        final int times = random.nextInt(10) + 1;
        for (int i = 0; i < times; i++) {
            commands += direction;
            commands += direction;
            commands += direction;
            commands += direction;
        }
        return commands;
    }

    private String generateNforwardCommand(int times) {
        String command = "";
        for (int i = 0; i < times; i++) {
            command += "M";
        }
        return command;
    }
}
